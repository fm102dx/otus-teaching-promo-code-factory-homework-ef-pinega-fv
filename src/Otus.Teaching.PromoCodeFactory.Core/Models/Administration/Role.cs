﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class EmployeeRole : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}