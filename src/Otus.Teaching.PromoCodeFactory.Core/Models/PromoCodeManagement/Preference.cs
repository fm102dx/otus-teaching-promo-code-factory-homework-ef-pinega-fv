﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class Preference  :BaseEntity    
    {
        public string Name { get; set; }
    }
}